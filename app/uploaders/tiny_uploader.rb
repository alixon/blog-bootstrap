# encoding: utf-8

class TinyUploader < CarrierWave::Uploader::Base

  include CarrierWave::RMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :small_thumb do
    process :resize_with_promotion => [50, 50]
  end

  version :medium_thumb do
    process :resize_with_promotion => [100, 100]
  end

  version :large_thumb do
    process :resize_with_promotion => [370, 370]
  end

  version :detail_preview do
    process :resize_with_promotion => [450, 338]
  end

  def resize_with_promotion(*args)
    resize_to_fit(*args)
    manipulate! do |image|
      # if model.promotion
        # bg_image = Magick::Image.read(model.bg_image_path).first
        # image.composite(bg_image, Magick::SouthWestGravity, -20, 10, Magick::OverCompositeOp)
      # end
      image
    end
  end

  def geometry
    if file
      img = ::Magick::Image::read(file.file).first
      [img.rows, img.columns]
    end
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
