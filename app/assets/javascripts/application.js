// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require tinymce-jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .



tinyMCE.init({
    selector: "textarea.tinymce",
    plugins: [
        "autolink lists link image uploadimage charmap textcolor",
        "visualblocks code fullscreen template",
        "insertdatetime media table contextmenu paste"
    ],
    templates: [ 
        {title: 'Some title 1', description: 'Some desc 1', content: 'My content'}, 
        {title: 'Some title 2', description: 'Some desc 2', url: '/subscription-two-column.html'} 
    ],
    image_advtab: true,
    image_list: [
        {title: 'Dog', value: 'mydog.jpg'},
        {title: 'Cat', value: 'mycat.gif'}
    ],
    uploadimage_form_url: '/'
})
